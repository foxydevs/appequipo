import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvocarPage } from './convocar.page';

describe('ConvocarPage', () => {
  let component: ConvocarPage;
  let fixture: ComponentFixture<ConvocarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvocarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvocarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
