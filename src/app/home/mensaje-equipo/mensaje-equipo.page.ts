import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
@Component({
  selector: 'app-mensaje-equipo',
  templateUrl: './mensaje-equipo.page.html',
  styleUrls: ['./mensaje-equipo.page.scss'],
})
export class MensajeEquipoPage implements OnInit {

  titulo:string = "Chat"
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
  }

}
