import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController } from '@ionic/angular';

import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

import { EquiposService } from "./../../_services/equipos.service";
@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.page.html',
  styleUrls: ['./equipos.page.scss'],
})
export class EquiposPage implements OnInit {
  titulo:string = "Equipos"
  Table: any;
  selectedData: any;
  private search: any;
  currentId:any = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    private mainServices: EquiposService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    
    this.route.params
              .switchMap((params: Params) => (params['id']))
              .subscribe(response => { 
                                this.currentId+=response
                            });
    this.getAll(this.currentId);
  }

  getAll(id){
    this.presentLoading("Cargando "+this.titulo);
    this.mainServices.getAll()
                      .then(response=>{
                        this.Table = response;
                        this.loadingController.dismiss();
                      }).catch(error=>{
                        console.log(error);
                        this.loadingController.dismiss();
                      })
  }

  async presentLoading(msg:string) {
    const loading = await this.loadingController.create({
      content: msg
    });
    return await loading.present();
  }

}
