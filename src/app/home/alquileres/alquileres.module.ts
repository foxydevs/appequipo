import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AlquileresPage } from './alquileres.page';
import { MenuPage } from './../menu/menu.page';

const routes: Routes = [
  {
    path: '',
    component: AlquileresPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    AlquileresPage,
    MenuPage
  ]
})
export class AlquileresPageModule {}
