import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
@Component({
  selector: 'app-alquileres',
  templateUrl: './alquileres.page.html',
  styleUrls: ['./alquileres.page.scss'],
})
export class AlquileresPage implements OnInit {
  titulo:string = "Alquileres"
  eventSource=[]
  vierTitle:string;
  selectedDay = new Date();

  calendar={
    mode:'month',
    urrentDate: this.selectedDay
  }
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location
  ) { }

  
  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
  }

}
