import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.page.html',
  styleUrls: ['./jugador.page.scss'],
})
export class JugadorPage implements OnInit {
  titulo:string = "Jugador"
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
  }

}
