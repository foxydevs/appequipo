import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { NoticiasService } from './../_services/noticias.service';
import { JugadoresService } from './../_services/jugadores.service';
import { TorneosService } from './../_services/torneos.service';
import { EquiposService } from './../_services/equipos.service';

import { HomePage } from './home.page';
import { TorneosPage } from "./torneos/torneos.page";
import { AlquileresPage } from "./alquileres/alquileres.page";
import { MenuPage } from "./menu/menu.page";
import { AmistososPage } from "./amistosos/amistosos.page";
import { FichajesPage } from "./fichajes/fichajes.page";
import { ChatPage } from "./chat/chat.page";
import { PartidosPage } from "./partidos/partidos.page";
import { EnvivoPage } from "./envivo/envivo.page";
import { EquiposPage } from "./equipos/equipos.page";
import { EquipoPage } from "./equipo/equipo.page";
import { LigaPage } from "./liga/liga.page";
import { JugadorPage } from "./jugador/jugador.page";
import { MensajesPage } from "./mensajes/mensajes.page";
import { MensajeEquipoPage } from "./mensaje-equipo/mensaje-equipo.page";
import { NotasPage } from "./notas/notas.page";
import { CalificarPage } from "./calificar/calificar.page";
import { ConvocarPage } from "./convocar/convocar.page";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'torneos',
        component: TorneosPage
      },
      {
        path: 'alquileres',
        component: AlquileresPage
      },
      {
        path: 'amistosos',
        component: AmistososPage
      },
      {
        path: 'fichajes',
        component: FichajesPage
      },
      {
        path: 'chat',
        component: ChatPage
      },
      {
        path: 'partidos',
        component: PartidosPage
      },
      {
        path: 'envivo',
        component: EnvivoPage
      },
      {
        path: 'liga/:id',
        component: LigaPage
      },
      {
        path: 'equipos/:id',
        component: EquiposPage
      },
      {
        path: 'equipo',
        component: EquipoPage
      },
      {
        path: 'jugador/:id',
        component: JugadorPage
      },
      {
        path: 'chat/mensaje/:id',
        component: MensajesPage
      },
      {
        path: 'equipo/:id/mensaje',
        component: MensajeEquipoPage
      },
      {
        path: 'notas/:id',
        component: NotasPage
      },
      {
        path: 'convocar/:id',
        component: NotasPage
      },
      {
        path: 'calificar/:id',
        component: NotasPage
      }
    ])
  ],
  declarations: [
    HomePage,
    TorneosPage,
    AlquileresPage,
    MenuPage,
    AmistososPage,
    FichajesPage,
    ChatPage,
    MensajesPage,
    EnvivoPage,
    PartidosPage,
    CalificarPage,
    EquiposPage,
    MensajeEquipoPage,
    LigaPage,
    JugadorPage,
    NotasPage,
    EquipoPage
  ],
  entryComponents: [
    TorneosPage,
    AlquileresPage,
    MenuPage,
    AmistososPage,
    FichajesPage,
    ChatPage,
    ConvocarPage,
    MensajesPage,
    EnvivoPage,
    PartidosPage,
    EquiposPage,
    MensajeEquipoPage,
    LigaPage,
    JugadorPage,
    NotasPage,
    EquipoPage
  ],
  providers: [
    NoticiasService,
    JugadoresService,
    TorneosService,
    EquiposService
  ]
})
export class HomePageModule {}
