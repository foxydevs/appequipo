import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmistososPage } from './amistosos.page';

describe('AmistososPage', () => {
  let component: AmistososPage;
  let fixture: ComponentFixture<AmistososPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmistososPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmistososPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
