import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController } from '@ionic/angular';

import { TorneosService } from "./../../_services/torneos.service";

@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.page.html',
  styleUrls: ['./torneos.page.scss'],
})
export class TorneosPage implements OnInit {
  titulo:string = "Torneos"
  Table: any;
  selectedData: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    private mainServices: TorneosService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.presentLoading("Cargando "+this.titulo);
    this.mainServices.getAll()
                      .then(response=>{
                        this.Table = response;
                        this.loadingController.dismiss();
                      }).catch(error=>{
                        console.log(error);
                        this.loadingController.dismiss();
                      })
  }

  async presentLoading(msg:string) {
    const loading = await this.loadingController.create({
      content: msg
    });
    return await loading.present();
  }
}
