import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NoticiasService } from "./../_services/noticias.service";
import { EquiposService } from "./../_services/equipos.service";
import { path } from "./../config.module";
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo:string = "Home"
  Table: any;
  selectedData: any;
  private baseId:string = path.id
  constructor(
    public navCtrl: NavController,
    private mainServices: NoticiasService,
    private parentService: EquiposService
  ) { }

  goToRoute(route:string) {
    this.navCtrl.goForward(route);
  }
  ngOnInit() {
    this.getAll();
  }
  getAll(){
    this.Table = [
      {
        descripcion:"Final copa A",
        fecha:"4 de junio",
        lugar:"Estadio zona 6",
        foto:null
      },
      {
        descripcion:"Academia de vacaciones",
        fecha:"24 de noviembre",
        lugar:"Inscripcion Abierta",
        foto:null
      },
      {
        descripcion:"Amistosos",
        fecha:"Titulos",
        lugar:"vs",
        foto:null
      }
    ]

    this.selectedData = 
      {
        nombre:"Foxylabs",
        id:1,
        lugar:"Estadio zona 6",
        foto:null
      }
    // this.mainServices.getAll()
    //                   .then(response=>{
    //                     console.log(response);
                        
    //                   }).catch(error=>{
    //                     console.log(error);
                        
    //                   })
  }
}
