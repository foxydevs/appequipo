import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Location } from '@angular/common';
import { LoadingController } from '@ionic/angular';

import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';

import { EquiposService } from "./../../_services/equipos.service";
import { JugadoresService } from "./../../_services/jugadores.service";
@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.page.html',
  styleUrls: ['./equipo.page.scss'],
})
export class EquipoPage implements OnInit {
  titulo:string = "Equipo"
  tabSelected:number = 1;
  Table: any;
  selectedData: any;
  private search: any;
  currentId:any = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location:Location,
    public loadingController: LoadingController,
    public childServices: JugadoresService,
    private mainServices: EquiposService
  ) { }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])

  }
  goToBack() {
    this.location.back();
  }

  ngOnInit() {
    this.route.params
              .switchMap((params: Params) => (params['id']))
              .subscribe(response => { 
                                this.currentId+=response
                            });
    this.getSingle(this.currentId);
  }
  getSingle(id){
    this.presentLoading("Cargando "+this.titulo);
    this.mainServices.getSingle(id)
                      .then(response=>{
                        this.selectedData = response;
                       setTimeout(() => {
                         this.loadingController.dismiss();
                       }, 300); 
                      }).catch(error=>{
                        console.log(error);
                       setTimeout(() => {
                         this.loadingController.dismiss();
                       }, 300); 
                      })
  }

  async presentLoading(msg:string) {
    const loading = await this.loadingController.create({
      content: msg
    });
    return await loading.present();
  }

}
